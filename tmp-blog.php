<?php /* Template Name: Blog */ ?>
<?php get_header(); ?>

<section class="emisionet small">
	<div class="grid-container">
		<div class="grid-x grid-margin-x">
			<?php 
			 $args = array(
			 	'post_type' => 'post',
		        'posts_per_page' => -1
		        );
		    $loop = new WP_Query( $args );
		     ?>
			<?php if($loop->have_posts()) : while ($loop->have_posts()) : $loop->the_post(); ?>
			<div class="cell medium-3">
				<a href="<?php the_permalink(); ?>" class="single-emision-holder">
					<?php the_post_thumbnail() ?>
					<span class="overlay"></span>
					<span class="title"><?php the_title(); ?></span>
				</a>
			</div> 
			 
			<?php endwhile;endif;  wp_reset_postdata(); ?>	
			 

		</div>
	</div>
</section>

<?php get_footer(); ?>