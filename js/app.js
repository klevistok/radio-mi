$(document).foundation();



  $(".show-canvas").click(function(){
    $(".off-canvas-menu").animate({"left":"0"},300);
    $(".canvas-container").css("overflow","visible");
    $('body').toggleClass('overflow');
    $(".header").css("overflow","visible");

  });
  $(".close-canvas , .off-canvas-menu li a").click(function(){
    $(".off-canvas-menu").animate({"left":"100%"},300);
    $(".canvas-container").css("overflow","hidden");
    $('body').toggleClass('overflow');
    $(".header").css("overflow","hidden");
  });

$('a[href*="#"]')
  // Remove links that don't actually link to anything
  .not('[href="#"]')
  .not('[href="#0"]')
  .click(function(event) {
    // On-page links
    if (
      location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') 
      && 
      location.hostname == this.hostname
    ) {
      // Figure out element to scroll to
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      // Does a scroll target exist?
      if (target.length) {
        // Only prevent default if animation is actually gonna happen
        event.preventDefault();
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 1000, function() {
          // Callback after animation
          // Must change focus!
          var $target = $(target);
          $target.focus();
          if ($target.is(":focus")) { // Checking if the target was focused
            return false;
          } else {
            $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
            $target.focus(); // Set focus again
          };
        });
      }
    }
  }); 



$('.author-box').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  infinite: true,
  autoplay: true,
  autoplaySpeed: 4000,
  arrows: false,
  dots: false,
  asNavFor: '.mixes-box'

});

$('.slider-wrapper').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  infinite: true,
  autoplay: true,
  autoplaySpeed: 4000,
  arrows: true,
  dots: true,
  asNavFor: '.mixes-box'

});

$('.mixes-box').slick({
  slidesToShow: 3,
  slidesToScroll: 1,
  arrows: true,
  dots: false,
  infinite: true,
  vertical: true,
  verticalSwiping: true,
  asNavFor: '.author-box'
});


$('.episode-slider').slick({
  slidesToShow: 3,
  slidesToScroll: 1,
  arrows: true,
  dots: false,
  infinite: true,
  vertical: true,
  verticalSwiping: true
});

$('.sports-slider').on('beforeChange', function(event, slick, currentSlide, nextSlide){
  $('[data-goto]').removeClass('active');
  $('[data-goto="'+nextSlide+'"]').addClass('active');
});


$('.single-mix-title').on('click', function(){
  gotoNumber = $(this).attr('data-slick-index');
  $('.mixes-box').slick('slickGoTo',gotoNumber);
})

































