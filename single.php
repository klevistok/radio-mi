<?php get_header(); ?>
<?php if(have_posts()) : while (have_posts()) : the_post(); ?>



 
<section class="post-content">
	<div class="grid-container">
		<div class="grid-x align-center">
			<div class="cell medium-9">
				<div class="title"><?php the_title(); ?></div>
				<?php the_content(); ?>
			</div>
		</div>
	</div>
</section>
	

<?php endwhile;endif; ?>
<?php get_footer(); ?>