<?php get_header(); ?>

<section class="main-slider">
	<div class="grid-container">
		<div class="grid-x">
			<div class="cell">
				<div class="slider-wrapper">
					<?php 
						if( have_rows('main_slider') ):

					    while( have_rows('main_slider') ) : the_row();

					         
					     ?>
					     	<div>
					     		<div class="single-slide">
						     		<?php if (get_sub_field('slider_link')) { ?>
						     			<a href="<?php the_sub_field('slider_link'); ?>">
						     				<img src="<?php the_sub_field('slider_image'); ?>" alt="">
						     			</a>
					     			<?php }  else { ?>
					     				<img src="<?php the_sub_field('slider_image'); ?>" alt="">
					     			<?php } ?>
						     	</div>
					     	</div>
					    
			        	<?php  
					    endwhile;endif;
		    		?>
					
				</div>
			</div>
		</div>
	</div>
</section>

<section class="emisionet">
	<div class="grid-container">
		<div class="grid-x">
			<div class="cell medium-12">
				<div class="default-title">Emisionet e Fundit</div>
			</div>
			<?php 
			 $args = array(
			 	'post_type' => 'emision',
		        'posts_per_page' => 3,
		        'category' => 'featured'
		        );
		    $loop = new WP_Query( $args );
		     ?>
			<?php if($loop->have_posts()) : while ($loop->have_posts()) : $loop->the_post(); ?>
			<div class="cell medium-4">
				<a href="<?php the_permalink(); ?>" class="single-emision-holder">
					<?php if (get_field('latest_emision')) { ?>
						<img src="<?php the_field('latest_emision'); ?>" alt="">
					<?php } else { ?>
					<?php the_post_thumbnail() ?>
					<?php } ?>
					<span class="overlay"></span>
					<span class="title"><?php the_title(); ?></span>
				</a>
			</div> 
			<?php endwhile;endif;  wp_reset_postdata(); ?>	
		</div>
	</div>
</section>
 

<section class="partners">
	<div class="grid-container">
		<div class="grid-x">
			<?php 
				if( have_rows('partners',5) ):

			    while( have_rows('partners',5) ) : the_row();

			        if (get_sub_field('partner_link')) {
			     ?>
			     	<div class="cell medium-3">
			        	<div class="partner-holder">
			        		<a href="<?php the_sub_field('partner_link'); ?>">
			        			<img src="<?php the_sub_field('partner_logo'); ?>" alt="">
			        		</a>
			        	</div>
			        </div>

			 	<?php } else { ?>

			        <div class="cell medium-3">
			        	<div class="partner-holder">
			        		<img src="<?php the_sub_field('partner_logo'); ?>" alt="">
			        	</div>
			        </div>
			    
	        	<?php 
			    	}
			    endwhile;endif;
		     ?>
		</div>
	</div>
</section>

<section class="emisionet small">
	<div class="grid-container">
		<div class="grid-x grid-margin-x">
		<div class="cell medium-12">
			<div class="default-title">Programi</div>
		</div>
			<?php 
			 $args = array(
			 	'post_type' => 'emision',
		        'posts_per_page' => -1
		        );
		    $loop = new WP_Query( $args );
		     ?>
			<?php if($loop->have_posts()) : while ($loop->have_posts()) : $loop->the_post(); ?>
			<div class="cell medium-3">
				<a href="<?php the_permalink(); ?>" class="single-emision-holder">
					<?php the_post_thumbnail() ?>
					<span class="overlay"></span>
					<span class="title"><?php the_title(); ?></span>
				</a>
			</div>  
			<?php endwhile;endif;  wp_reset_postdata(); ?>	 

		</div>
	</div>
</section>


<section class="media-coverage">
	<div class="grid-container">
		<div class="grid-x">
			<div class="cell medium-6">
				<div class="big-title">
					Mi <br>Media
				</div>
			</div>
			<div class="cell medium-6">
				<div class="content">
					<?php the_field('mc_content'); ?>
					<a href="<?php echo site_url(); ?><?php the_field('mc_linkk'); ?>" class="default-button"><?php the_field('mc_button_text'); ?></a>
				</div>
			</div>
		</div>
	</div>
</section>


<section class="mixes">
	<div class="grid-container">
		<div class="grid-x">
			<div class="cell medium-6">
				<div class="author-box">
					<?php 
					 $args = array(
					 	'post_type' => 'mix',
				        'posts_per_page' => -1
				        );
				    $loop = new WP_Query( $args );
				     ?>
					<?php if($loop->have_posts()) : while ($loop->have_posts()) : $loop->the_post(); ?>
					<div>
						<div class="single-author">
							<div class="grid-x grid-padding-x">
								<div class="cell medium-5">
									<?php if (get_field('author_photo')){ ?>
										<img src="<?php the_field('author_photo'); ?>" alt="">
									<?php } ?>
								</div>
								<div class="cell medium-7">
									<div class="single-mix">
										<div class="mix-name"><?php the_title(); ?>
											<br><span>by <?php the_field('author_name'); ?></span>
										</div>
										<div class="iframe-holder">
											<?php the_field('embedded_link'); ?> 
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<?php endwhile;endif;  wp_reset_postdata(); ?>
				</div>
			</div>
			<div class="cell medium-6">
				<div class="mixes-box">
					<?php 
					 $args = array(
					 	'post_type' => 'mix',
				        'posts_per_page' => -1
				        );
				    $loop = new WP_Query( $args );
				     ?>
					<?php if($loop->have_posts()) : while ($loop->have_posts()) : $loop->the_post(); ?>
					<div class="single-mix-title">
						<div class="wrapper">
							<span class="title"><?php the_title(); ?></span>
							<span class="author">by <?php the_field('author_name'); ?></span>
						</div> 
					</div>
					<?php endwhile;endif;  wp_reset_postdata(); ?>	
				</div>
			</div>
		</div>
	</div>
</section>

<?php get_footer(); ?>

