<?php /* Template Name: Media Coverage */ ?>
<?php get_header(); ?>

<section class="media-coverage">
	<div class="grid-container">
		<div class="grid-x">
			<div class="cell medium-6">
				<div class="big-title">
					<?php the_title(); ?>
				</div>
			</div>
			<div class="cell medium-6">
				<div class="content">
					<?php the_content(); ?>
					<a href="#contact-us" class="default-button">Contact Us</a>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="our-works">
	<div class="grid-container">
		<?php 
		 $args = array(
		 	'post_type' => 'work',
	        'posts_per_page' => -1
	        );
	    $loop = new WP_Query( $args );
	     ?>
		<?php if($loop->have_posts()) : while ($loop->have_posts()) : $loop->the_post(); ?>
			<div class="single-work">
				<div class="grid-x grid-padding-x">
					<div class="cell medium-4">
						<div class="work-image">
							<?php the_post_thumbnail(); ?>
						</div>
					</div>
					<div class="cell medium-8">
						<div class="work-content">
							<div class="wrapper">
								<div class="title"><?php the_title(); ?></div>
								<?php the_excerpt(); ?><br>
								<a href="<?php the_permalink(); ?>" class="default-button">View</a>
							</div>
						</div>
					</div>
				</div>
			</div> 
		<?php endwhile;endif;  wp_reset_postdata(); ?>	
	</div>
</section>

<?php get_footer(); ?>