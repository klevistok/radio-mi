<?php get_header(); ?>
<?php if(have_posts()) : while (have_posts()) : the_post(); ?>

<section class="media-coverage emision-intro">
	<div class="grid-container">
		<div class="grid-x">
			<div class="cell medium-8">
				<div class="big-title">
					<?php the_title(); ?>
					<span><?php the_field('author_name') ?></span>
				</div>
				<div class="content">
					<div class="time-table">
						<?php the_field('time_table') ?>
					</div>
				</div>
			</div>
			<div class="cell medium-4">
				<img src="<?php the_field('banner') ?>" alt="">	
			</div>
		</div>
	</div>
</section>

<section class="post-content">
	<div class="grid-container">
		<div class="grid-x align-center">
			<div class="cell medium-12">
				<?php the_content(); ?>
			</div>
		</div>
	</div>
</section>

<section class="episodet">
	<div class="grid-container">
		<div class="grid-x">
			<div class="cell medium-12">
				<div class="episode-slider">
					<?php 
						if( have_rows('episodet') ):

					    while( have_rows('episodet') ) : the_row();

					         
					     ?>
					     	<div>
					     		<div class="single-episode">
						     		<div class="grid-x">
							     		<div class="cell medium-9">
								        	<?php the_sub_field('embedded_link') ?>
								        </div> 
								        <div class="cell medium-3">
								        	<div class="title">
								        		<?php the_sub_field('episodi') ?>
								        	</div>
								        </div>
							     	</div>
						     	</div>
					     	</div>
					    
			        	<?php  
					    endwhile;endif;
		    		?>
				</div>
			</div>
		</div>
	</div>
</section>

<?php endwhile;endif; ?>
<?php get_footer(); ?>