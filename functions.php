<?php

add_theme_support( 'menus' );
add_theme_support( 'widgets' );
add_theme_support( 'post-thumbnails' ); 
 

 function tok_scripts() {

    wp_enqueue_style( 'google-fonts', 'https://fonts.googleapis.com/css?family=Cinzel:400,700,900&display=swap', '', '', false);
    wp_enqueue_style( 'wow-stylesheet', get_stylesheet_directory_uri() . '/scss/animate.css?v' . time(), '', '', false);
    wp_enqueue_style( 'main-stylesheet', get_stylesheet_directory_uri() . '/css/app.css?v' . time(), '', '', false);
    wp_enqueue_style( 'lightbox2-stylesheet', get_stylesheet_directory_uri() . '/node_modules/lightbox2/dist/css/lightbox.min.css', '', '', false);
    wp_enqueue_style( 'slick-carousel-stylesheet', get_stylesheet_directory_uri(). '/node_modules/slick-carousel/slick/slick.css', '', '', false);

    wp_deregister_script('jquery');

    wp_enqueue_script( 'jquery', get_stylesheet_directory_uri(). '/node_modules/jquery/dist/jquery.min.js', array(), '', false);

    wp_enqueue_script( 'what-input', get_stylesheet_directory_uri(). '/node_modules/what-input/dist/what-input.min.js', array(), '', true);
    wp_enqueue_script( 'foundation', get_stylesheet_directory_uri(). '/node_modules/foundation-sites/dist/js/foundation.min.js', array('jquery'), '', true );
    wp_enqueue_script( 'slick-carousel', get_stylesheet_directory_uri(). '/node_modules/slick-carousel/slick/slick.min.js', array('jquery'), '', true);
    wp_enqueue_script( 'lightbox2', get_stylesheet_directory_uri(). '/node_modules/lightbox2/dist/js/lightbox.min.js', array('jquery'), '', true);
    wp_enqueue_script( 'wow-scripts', get_stylesheet_directory_uri(). '/js/wow.min.js?v' . time(), array(), '', true );
    wp_enqueue_script( 'tok-scripts', get_stylesheet_directory_uri(). '/js/app.js?v' . time(), array(), '', true );

}


add_action( 'wp_enqueue_scripts', 'tok_scripts' );


function custom_posttype() {
	register_post_type( 'emision', 
		array(
			'labels' => array(
				'name' => __( 'Emisione' ),
				'singular_name' => __( 'emision' )
			),
			'public' => true,
			'has_archive' => true,
			'supports' => array( 'thumbnail', 'title', 'editor','excerpt' ),
			'taxonomies'  => array( 'emision-category' )
		)
	);

    register_post_type( 'work', 
        array(
            'labels' => array(
                'name' => __( 'Works' ),
                'singular_name' => __( 'work' )
            ),
            'public' => true,
            'has_archive' => true,
            'supports' => array( 'thumbnail', 'title', 'editor','excerpt' ),
            'taxonomies'  => array( 'work-category' )
        )
    );

    register_post_type( 'mix', 
        array(
            'labels' => array(
                'name' => __( 'Mixes' ),
                'singular_name' => __( 'mix' )
            ),
            'public' => true,
            'has_archive' => true,
            'supports' => array( 'thumbnail', 'title', 'editor','excerpt' ),
            'taxonomies'  => array( 'mix-category' )
        )
    );
}
add_action( 'init', 'custom_posttype' );

function custom_taxonomies() {

    register_taxonomy(
        'emision-category',
        'emision',
        array(
            'label' => __( 'Kategorite' ),
            'rewrite' => array( 'slug' => 'emision-category' ),
            'hierarchical' => true,
        )
    );

    register_taxonomy(
        'work-category',
        'work',
        array(
            'label' => __( 'Kategorite' ),
            'rewrite' => array( 'slug' => 'work-category' ),
            'hierarchical' => true,
        )
    );

    register_taxonomy(
        'mix-category',
        'mix',
        array(
            'label' => __( 'Kategorite' ),
            'rewrite' => array( 'slug' => 'mix-category' ),
            'hierarchical' => true,
        )
    );
}
add_action( 'init', 'custom_taxonomies' );