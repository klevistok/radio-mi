<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="apple-touch-icon" sizes="57x57" href="<?php bloginfo('template_url'); ?>/fav/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php bloginfo('template_url'); ?>/fav/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php bloginfo('template_url'); ?>/fav/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php bloginfo('template_url'); ?>/fav/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php bloginfo('template_url'); ?>/fav/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php bloginfo('template_url'); ?>/fav/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php bloginfo('template_url'); ?>/fav/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php bloginfo('template_url'); ?>/fav/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php bloginfo('template_url'); ?>/fav/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?php bloginfo('template_url'); ?>/fav/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php bloginfo('template_url'); ?>/fav/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php bloginfo('template_url'); ?>/fav/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php bloginfo('template_url'); ?>/fav/favicon-16x16.png">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?php bloginfo('template_url'); ?>/fav/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <title><?php the_title(); ?> | RADIO MI</title>
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,600,700" rel="stylesheet">
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/app.css">
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <script src="https://use.fontawesome.com/4b5b77d0bd.js"></script>
     <?php wp_head() ?>
  </head>
  <body>
    <img src="<?php bloginfo('template_url') ?>/img/circle.png" alt="" class="circle">
    <img src="<?php bloginfo('template_url') ?>/img/square.png" alt="" class="square">
    <img src="<?php bloginfo('template_url') ?>/img/triangle.png" alt="" class="triangle">
    
    <div class="canvas-container">
        <div class="off-canvas-menu">
            <div class="off-canvas-inner">
                <button class="close-canvas" aria-label="Close menu" >
                  <span aria-hidden="true">&times;</span>
                </button>
                <div class="logo"><a href="<?php echo site_url() ?>"><img src="<?php bloginfo('template_url'); ?>/img/logo.svg"></a></div>
                    <?php wp_nav_menu(
                    array(
                        'menu'=>'Main Menu'
                    )); ?>
            </div>
        </div>
    </div>
    <div class="header" id="scroll-menu">
        <div class="main-navigation">
            <div class="grid-container">
                <div class="grid-x grid-padding-x">
                    <div class="small-3 medium-2 cell">
                        <ul class="logo-container">
                            <li class="logo"><a href="<?php echo site_url() ?>"><img src="<?php bloginfo('template_url'); ?>/img/logo.svg"></a></li>
                        </ul> 
                    </div>  
                    <div class="cell small-6 medium-3">
                        <div class="player-holder">
                            <audio id="musicPlayer" controls> 
                                <source src="https://streamingv2.shoutcast.com/radio-mi-online" type="audio/ogg">
                                <source src="https://streamingv2.shoutcast.com/radio-mi-online" type="audio/mpeg">
                            </audio>
                        </div>
                        <div class="hide-for-large show-canvas"><i class="fa fa-bars" aria-hidden="true"></i></div>
                    </div> 
                    <div class="cell small-3 medium-7">
                        <ul class="menu main-menu show-for-large">
                            <?php wp_nav_menu(
                                array(
                                    'menu'=>'Main Menu'
                            )); ?> 
                        </ul> 
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="header-fix">
    </div>
       