

<?php if (is_front_page()){ ?>   
<?php } else { ?>
	<section class="partners">
		<div class="grid-container">
			<div class="grid-x">
				<?php 
					if( have_rows('partners',5) ):

				    while( have_rows('partners',5) ) : the_row();

				        if (get_sub_field('partner_link')) {
				     ?>
				     	<div class="cell medium-3">
				        	<div class="partner-holder">
				        		<a href="<?php the_sub_field('partner_link'); ?>">
				        			<img src="<?php the_sub_field('partner_logo'); ?>" alt="">
				        		</a>
				        	</div>
				        </div>

				 	<?php } else { ?>

				        <div class="cell medium-3">
				        	<div class="partner-holder">
				        		<img src="<?php the_sub_field('partner_logo'); ?>" alt="">
				        	</div>
				        </div>
				    
		        	<?php 
				    	}
				    endwhile;endif;
			     ?>
			</div>
		</div>
	</section>

	<section class="emisionet small">
		<div class="grid-container">
			<div class="grid-x grid-margin-x">
				<?php 
				 $args = array(
				 	'post_type' => 'emision',
			        'posts_per_page' => -1
			        );
			    $loop = new WP_Query( $args );
			     ?>
				<?php if($loop->have_posts()) : while ($loop->have_posts()) : $loop->the_post(); ?>
				<div class="cell medium-3">
					<a href="<?php the_permalink(); ?>" class="single-emision-holder">
						<?php the_post_thumbnail() ?>
						<span class="overlay"></span>
						<span class="title"><?php the_title(); ?></span>
					</a>
				</div>  
				<?php endwhile;endif;  wp_reset_postdata(); ?>	 

			</div>
		</div>
	</section>
<?php } ?>
<div class="footer" id="footer">

	<section id="contact-us" class="contact-us">
		<div class="grid-container">
			<div class="grid-x align-center">
				<div class="cell medium-6">
					<div class="big-title">Na Kontaktoni</div>
					<div class="contact-form">
						<?php echo do_shortcode('[ninja_form id=1]') ?>
					</div>
				</div>
			</div>
		</div>
	</section>
	<div class="grid-container">
		<div class="grid-x grid-padding-x">
			<div class="cell medium-3">
				<div class="footer-logo">
					<img src="<?php bloginfo('template_url') ?>/img/logo.svg" alt="">
				</div>
			</div>
			<div class="cell medium-3">
				<div class="footer-info">
					<div class="footer-title">Adresa</div>
					<p>Rruga Abdyl Frasheri,<br>EGT TOWER, Kati 6, Hyrja 13<br>Tirana, Albania<br></p>
					<div class="footer-title">Telefon</div>
					<p>+355696066664</p>
				</div>
			</div>
			<div class="cell medium-3">
				<div class="footer-info">
					<div class="footer-title">Menu</div>
					<?php wp_nav_menu(
	                array(
	                    'menu'=>'Main Menu'
	                )); ?>
				</div>
			</div>
			<div class="cell medium-3">
				<div class="footer-info">
					<div class="footer-title">Rrjete sociale</div>
				</div>
				<div class="socials">
					<a href="https://www.facebook.com/radiomi.al/" target="_blank"><i class="fa fa-facebook-square" aria-hidden="true"></i></a>
	                <a href="https://www.instagram.com/radiomi.al/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a>
	                <a href="https://soundcloud.com/radio-mi" target="_blank"><i class="fa fa-soundcloud" aria-hidden="true"></i></a>
				</div>
			</div>
		</div>
	</div>
	
	<div class="grid-container">
		<div class="grid-x grid-padding-x">
			<div class="cell">
				<div class="copyright">
					<div class="the-copyright">RadioMi Copyright © 2020 - Powered by <a href="https://tok.al" target="_blank">TOK</a></div>
				</div>
			</div>
		</div>
	</div>
</div> 


 
    <?php wp_footer() ?>
  </body>
</html>